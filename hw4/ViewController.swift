//
//  ViewController.swift
//  hw4
//
//  Created by Dmytro Zhylich on 17.04.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // task - 1
        myNameIs()

        // task - 2
        myPatronymic()

        // task - 3
        firstnameLastname()

        // task - 4
        customReverse(myString: "Какой-то текст")

        // task - 5
        commaInLine(number: "123456700122", startText: true, lastText: false)
        commaInLine(number: "12345670012", startText: false, lastText: false)
        commaInLine(number: "1234567002", startText: false, lastText: true)

        // task - 6
        passwordVerification(pass: "123456")
        passwordVerification(pass: "qwertyui")
        passwordVerification(pass: "12345qwerty")
        passwordVerification(pass: "32556reWDr")
        passwordVerification(pass: "32556re&#WDr")

        // task - 7
        sortingAndDeleteDuplicate(line: [9, 1, 2, 5, 1, 7])

        // task - 8
        translit(testLine: "ЯЗЗЬ")
        translit(testLine: "морДа")

        // task - 9
        sample(listExamples: ["lada", "sedan", "baklazhan"], searchText: "da")
        
        // task - 10
        antimat(text: "hello my fak", replaceWords: ["fuck", "fak"])
        antimat(text: "hello my fak and fuck", replaceWords: ["fuck", "fak"])
        antimat(text: "hello my fuck", replaceWords: ["fuck", "fak"])
        antimat(text: "fak fak fuck - and fak fak fak", replaceWords: ["fuck", "fak"])
        antimat(text: "fuck – 4, ass – 3, bitch - 5", replaceWords: ["fuck", "ass", "bitch"])
    }
    
    
    func antimat(text: String, replaceWords: [String]) {
        print("Task - 10")
        print("Для программы antimat – исключить из предложения все слова содержащиеся в сете")
        
        var result: String = text
        for i in replaceWords {
            if result.contains(i) {
                let count_words = i.count
                result = result.replacingOccurrences(of: i, with: String(repeating: "*", count: count_words))
            }
        }
        
        print("Ввели строку:", text)
        print("Результат:", result)
        
        print("************************************\n")
    }
    
    
    func sample(listExamples: [String], searchText: String) {
        print("Task - 9")
        print("Сделать выборку из массива строк в которых содержится указанная строка")
        var result = [String]()
        
        for word in listExamples {
            if word.contains(searchText) {
                result.append(word)
            }
        }
        
        print("Символы: `\(searchText)` найдены в таких строках:", result)
        
        print("************************************\n")
    }
    
    
    func translit(testLine: String) {
        print("Task - 8")
        print("Написать метод, который будет переводить строку в транслит.")
        
        var symbolMap: [String: String] = ["а":"a","б":"b","в":"v","г":"g","д":"d","е":"e","ё":"yo", "ж":"zh","з":"z","и":"i","й":"i","к":"k","л":"l","м":"m","н":"n", "о":"o","п":"p","р":"r","с":"s","т":"t","у":"u","ф":"f","х":"h", "ц":"c","ч":"ch","ш":"sh","щ":"sch","ъ":"","ы":"y","ь":"","э":"e", "ю":"u","я":"ya", "А":"A","Б":"B","В":"V","Г":"G","Д":"D","Е":"E","Ё":"YO", "Ж":"ZH","З":"Z","И":"I","Й":"I","К":"K","Л":"L","М":"M","Н":"N", "О":"O","П":"P","Р":"R","С":"S","Т":"T","У":"U","Ф":"F","Х":"H","Ц":"C","Ч":"CH","Ш":"SH","Щ":"SCH","Ъ":"","Ы":"y","Ь":"","Э":"E", "Ю":"U","Я":"YA",",":"","?":"","~":"","!":"","@":"","#":"", "$":"","%":"","^":"","&":"","*":"","(":"",")":"","-":"","=":"","+":"", ":":"",";":"","<":"",">":"","\\":"","/":"","№":"","[":"","]":"","{":"","}":"","ґ":"","ї":"","є":"","Ґ":"g","Ї":"i","Є":"e","—":""]
        
        var result = ""
        for i in testLine {
            if let symbolEng = symbolMap[String(i)] {
                result += symbolEng
            }
        }
        
        print("Ввели", testLine)
        print("Конвертировали", result)
        
        print("************************************\n")
    }
    
    
    func sortingAndDeleteDuplicate(line: Set<Int>) {
        print("Task - 7")
        print("Сортировка массива не встроенным методом по возрастанию + удалить дубликаты")
        
        var result = [Int]()
        
        for i in line {
            if result.contains(i) {
                continue
            }
            
            if result.isEmpty {
                result.append(i)
            } else {
                var appended = false
                
                for j in result {
                    if i < j {
                        let index = result.firstIndex(of: j)!
                        result.insert(i, at: index)
                        appended = true
                        break
                    }
                }
                
                if !appended {
                    result.append(i)
                }
            }
        }
        
        print("После сортировки получили: \(result)")
        
        print("************************************\n")
    }
    
    
    func passwordVerification(pass: String) -> Bool {
        print("Task - 6")
        print("Проверить пароль на надежность от 1 до 5")
        
        if pass.isEmpty {
            return false
        }
        
        var result: Int = 0
        
        let char = pass.first { char -> Bool in
            return char.isNumber
        }
        if char != nil {
            result += 1
        }
        
        let upper = pass.first { char -> Bool in
            return char.isUppercase
        }
        if upper != nil {
            result += 1
        }
        
        let lower = pass.first { char -> Bool in
            return char.isLowercase
        }
        if lower != nil {
            result += 1
        }
        
        let symbol = pass.first { char -> Bool in
            return char.isPunctuation
        }
        if symbol != nil {
            result += 1
        }
        
        var verdict: String = ""
        if result == 4 {
            verdict = "валидный"
        } else {
            verdict = "НЕ валидный"
        }
        
        print("В этом пароле `\(pass)` балов набрано: \(result). Пароль \(verdict)")
        
        print("************************************\n")
        return result == 4
    }
    
    
    func commaInLine(number: String, startText: Bool, lastText: Bool) {
        if startText {
            print("Task - 5")
            print("Добавить запятые в строку как их расставляет калькулятор: 1234567 → 1,234,567")
        }
        
        var result = ""
        var numberStr = number
        for i in 1...number.count {
            let Word = numberStr.popLast()
            let strWord = Word!
            
            if i % 3 == 0 {
                if i == number.count {
                     result = "\(strWord)" + result
                } else {
                     result = ",\(strWord)" + result
                }
            } else {
                 result = "\(strWord)" + result
            }
        }
        
        print("Ввели:", number)
        print("Преобразовали:", result)
        
        if lastText {
            print("************************************\n")
        } else {
            print("\n")
        }
    }
    
    
    func customReverse(myString: String) {
        print("Task - 4")
        print("Вывести строку зеркально Ось → ьсО не используя reverse (посимвольно)")
        
        var result: String = ""
        for i in myString {
            result.insert(i, at: result.startIndex)
        }
        
        print("** Введен текст: ", myString)
        print("Текст на оборот: ", result)
        
        print("************************************\n")
    }
    
    
    func myNameIs(){
        print("Task - 1")
        print("Создать строку с своим именем, вывести количество символов содержащихся в ней.")
        
        let val = "Дмитрий"
        print("Длинна имени: \(val.count)")
        
        print("************************************\n")
    }
    
    
    func myPatronymic(){
        print("Task - 2")
        print("Создать строку с своим отчеством проверить его на окончание “ич/на”")
        
        let val = "Викторович"
        if val.hasSuffix("ич") {
            print("ДА, “ич“ найден!!!")
        } else if val.hasSuffix("на") {
            print("Круто, “на“ найдено! Мои поздравления.")
        } else {
            print("В вашем отчестве не нашли нужных букв. Попробуйте позже.")
        }
        
        print("************************************\n")
    }
    
    
    func firstnameLastname(){
        print("Task - 3")
        let str1 = "Дмитрий"
        let str2 = "Жилич"
        
        let tmp = "\(str1)\(str2)"
        
        print("Cоздать строку, где слитно написано Ваши ИмяФамилия “\(tmp)” и дальше разбить ее с пробелом.")

        print("Имя и Фамилия вместе:", tmp)
        
        var fullNameWithSpase: String = ""
        for i in tmp {
            if i.isUppercase && fullNameWithSpase.count != 0 {
                fullNameWithSpase.append(" ")
            }
            fullNameWithSpase.append(i)
        }
        
        print("Тут вторая часть, разбира с пробелом: ", fullNameWithSpase)
        
        print("************************************\n")
    }
}
